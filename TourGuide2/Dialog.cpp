#include "Dialog.h"
#include "ui_Dialog.h"

Dialog::Dialog(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Dialog)
{
    ui->setupUi(this);
}

Dialog::~Dialog()
{
    delete ui;
}

QPushButton* Dialog::pushButton_1()
{
    return ui->pushButton_1;
}

QPushButton* Dialog::pushButton_2()
{
    return ui->pushButton_2;
}

QPushButton* Dialog::pushButton_3()
{
    return ui->pushButton_3;
}

QPushButton* Dialog::pushButton_4()
{
    return ui->pushButton_4;
}

QPushButton* Dialog::pushButton_5()
{
    return ui->pushButton_5;
}

