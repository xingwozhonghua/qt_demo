#ifndef TOURGUIDE_H
#define TOURGUIDE_H

#include <QWidget>
#include <QPaintEvent>
#include <QPainter>
#include <QPalette>
#include <QBrush>
#include "GuidancePopup.h"

class TourGuide : public QWidget
{
    Q_OBJECT
public:
    explicit TourGuide(QWidget* w, QWidget *parent = 0);
    void onAddWidgetStep(QList<QWidget*> widgets, QStringList contentList, int steps);

protected:
    void paintEvent(QPaintEvent *event);
    void mousePressEvent(QMouseEvent *event);
protected slots:
    void slot_mouseButtonPressed();
private:
    //绘制弹窗
    void drawPopup(QPainter& painter, int step);
private:
    QList<QList<QWidget*>> m_ListWidgetSteps;
    QList<QStringList> m_ListWidgetStrList;
    QList<int> m_ListWidgetIndex;

    int m_index = 0;

    QWidget* m_MainWindows = nullptr;
    QLine targetLine;
    GuidancePopup* m_GuidancePopup = nullptr;
};

#endif // TOURGUIDE_H

