#include "GuidancePopup.h"
#include "ui_GuidancePopup.h"
#include <QMouseEvent>
GuidancePopup::GuidancePopup(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::GuidancePopup)
{
    ui->setupUi(this);
    QObject::connect(ui->next, &QPushButton::clicked,
                     this, &GuidancePopup::signal_mouseButtonPressed);
}

GuidancePopup::~GuidancePopup()
{
    delete ui;
}

void GuidancePopup::setTitle(QString title)
{
    ui->label->setText(title);
}

void GuidancePopup::setStep(int step,int count)
{
    ui->next->setText(QString::number(step + 1)+"/"+QString::number(count)+" "+tr("Next"));
}

void GuidancePopup::mousePressEvent(QMouseEvent *event)
{
    if (event->type() == QMouseEvent::MouseButtonPress) {
        emit signal_mouseButtonPressed();
    }
}
