#include "TourGuide.h"
#include "Dialog.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    Dialog* dialog = new Dialog;
    dialog->show();
    TourGuide* m_T = new TourGuide(dialog, nullptr);
    m_T->setParent(dialog);
    QList<QWidget*> widgets0;
    widgets0 <<(QWidget*)dialog->pushButton_1() <<(QWidget*)dialog->pushButton_2() <<(QWidget*)dialog->pushButton_3();
    m_T->onAddWidgetStep(widgets0, QStringList() << "它是第1个", 0);

    QList<QWidget*> widgets1;
    widgets1 <<(QWidget*)dialog->pushButton_1() <<(QWidget*)dialog->pushButton_2() ;
    m_T->onAddWidgetStep(widgets1, QStringList() << "它是第2个", 1);
    m_T->setFixedSize(dialog->size());
    m_T->show();
    return a.exec();
}
