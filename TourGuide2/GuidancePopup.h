#ifndef GUIDANCEPOPUP_H
#define GUIDANCEPOPUP_H

#include <QDialog>

namespace Ui {
class GuidancePopup;
}

class GuidancePopup : public QDialog
{
    Q_OBJECT

public:
    explicit GuidancePopup(QWidget *parent = nullptr);
    ~GuidancePopup();
    void setTitle(QString title);
    void setStep(int step,int count);

protected:
    void mousePressEvent(QMouseEvent *event);

signals:
    void signal_mouseButtonPressed();

private:
    Ui::GuidancePopup *ui;
};

#endif // GUIDANCEPOPUP_H
