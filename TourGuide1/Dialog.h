#ifndef DIALOG_H
#define DIALOG_H

#include <QDialog>

namespace Ui {
class Dialog;
}

class Dialog : public QDialog
{
    Q_OBJECT

public:
    explicit Dialog(QWidget *parent = nullptr);
    ~Dialog();
    QPushButton* pushButton_1();
    QPushButton* pushButton_2();
    QPushButton* pushButton_3();
    QPushButton* pushButton_4();
    QPushButton* pushButton_5();
private:
    Ui::Dialog *ui;
};

#endif // DIALOG_H
