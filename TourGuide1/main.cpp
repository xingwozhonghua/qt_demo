#include "TourGuide.h"
#include "Dialog.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    Dialog* dialog = new Dialog;
    dialog->show();
    TourGuide* m_T = new TourGuide(dialog, nullptr);
    m_T->setParent(dialog);
    m_T->onAddWidget((QWidget*)dialog->pushButton_1(), "这是第1个按钮", QStringList() << "他是第1个", 0);
    m_T->onAddWidget((QWidget*)dialog->pushButton_2(), "这是第2个按钮", QStringList() << "他是第2个", 1);
    m_T->onAddWidget((QWidget*)dialog->pushButton_3(), "这是第3个按钮", QStringList() << "他是第3个", 2);
    m_T->onAddWidget((QWidget*)dialog->pushButton_4(), "这是第4个按钮", QStringList() << "他是第4个", 3);
    m_T->onAddWidget((QWidget*)dialog->pushButton_5(), "这是第5个按钮", QStringList() << "他是第5个", 4);
    m_T->setFixedSize(dialog->size());
    m_T->show();
    return a.exec();
}
