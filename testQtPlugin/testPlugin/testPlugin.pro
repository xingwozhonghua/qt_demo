CONFIG += plugin
CONFIG(release, debug|release): LIBDIR = $$PWD/dll/release
CONFIG(debug, debug|release): LIBDIR = $$PWD/dll/debug

TARGET      = $$qtLibraryTarget(testpluginplugin)
DESTDIR     = $$LIBDIR

TEMPLATE    = lib

HEADERS     = testpluginplugin.h
SOURCES     = testpluginplugin.cpp
RESOURCES   = icons.qrc
LIBS        += -L. 

greaterThan(QT_MAJOR_VERSION, 4) {
    QT += designer
} else {
    CONFIG += designer
}

target.path = $$[QT_INSTALL_PLUGINS]/designer
INSTALLS    += target

include(testplugin.pri)
