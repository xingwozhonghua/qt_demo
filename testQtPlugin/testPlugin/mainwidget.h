#ifndef MAINWIDGET_H
#define MAINWIDGET_H

#include <QWidget>

namespace Ui {
class mainwidget;
}

class QTimer;
class mainwidget : public QWidget
{
    Q_OBJECT

public:
    explicit mainwidget(QWidget *parent = nullptr);
    ~mainwidget();

public slots:
    void ResetTimer();
protected slots:
    void timerUpDate();

private:
    Ui::mainwidget *ui;
    QTimer *timer;
};

#endif // MAINWIDGET_H
