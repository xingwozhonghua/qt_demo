#include "mainwidget.h"
#include "ui_mainwidget.h"
#include <QTimer>
#include<QDateTime>

mainwidget::mainwidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::mainwidget)
{
    ui->setupUi(this);
    timer = new QTimer(this);
       //新建定时器
     connect(timer,SIGNAL(timeout()),this,SLOT(timerUpDate()));
       //关联定时器计满信号和相应的槽函数
     timer->start(1000);
}

mainwidget::~mainwidget()
{
    delete ui;
}

void mainwidget::ResetTimer()
{
    if( timer->isActive() )
    {
        timer->stop();
    }
    else
    {
        timer->start(1000);
    }
}

void mainwidget::timerUpDate()
{
    QDateTime time = QDateTime::currentDateTime();//获取系统现在的时间
    QString str = time.toString("yyyy-MM-dd hh:mm:ss ddd"); //设置显示格式
    QString strr= strr.fromLocal8Bit("current time:")+str;//调用中文显示
    ui->label->setText(strr);
}
