#include "mainwidget.h"
#include "testpluginplugin.h"

#include <QtPlugin>

testpluginPlugin::testpluginPlugin(QObject *parent)
    : QObject(parent)
{
    m_initialized = false;
}

void testpluginPlugin::initialize(QDesignerFormEditorInterface * /* core */)
{
    if (m_initialized)
        return;

    // Add extension registrations, etc. here

    m_initialized = true;
}

bool testpluginPlugin::isInitialized() const
{
    return m_initialized;
}

QWidget *testpluginPlugin::createWidget(QWidget *parent)
{
    return new mainwidget(parent);
}

QString testpluginPlugin::name() const
{
    return QLatin1String("testplugin");
}

QString testpluginPlugin::group() const
{
    return QLatin1String("");
}

QIcon testpluginPlugin::icon() const
{
    return QIcon();
}

QString testpluginPlugin::toolTip() const
{
    return QLatin1String("");
}

QString testpluginPlugin::whatsThis() const
{
    return QLatin1String("");
}

bool testpluginPlugin::isContainer() const
{
    return false;
}

QString testpluginPlugin::domXml() const
{
    return QLatin1String("<widget class=\"testplugin\" name=\"testplugin\">\n</widget>\n");
}

QString testpluginPlugin::includeFile() const
{
    return QLatin1String("testplugin.h");
}
#if QT_VERSION < 0x050000
Q_EXPORT_PLUGIN2(testpluginplugin, testpluginPlugin)
#endif // QT_VERSION < 0x050000
