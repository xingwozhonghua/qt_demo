#ifndef TOURGUIDE_H
#define TOURGUIDE_H

#include <QWidget>
#include <QPaintEvent>
#include <QPainter>
#include <QPalette>
#include <QBrush>


enum WSAD_POS {
    TOP_POS,
    DOWN_PPOS,
    LEFT_POS,
    RIGHT_POS
};
class TourGuide : public QWidget
{
    Q_OBJECT
public:
    explicit TourGuide(QWidget* w, QWidget *parent = 0);

    void onAddWidget(QWidget* w, QString title, QStringList contentList, int pos);
signals:

public slots:

protected:
    void paintEvent(QPaintEvent *event);
    void mousePressEvent(QMouseEvent *event);
private:
    //绘制箭头 WSAD绘制方向
    void drawArrows(QPainter& painter, QPoint pos, QRect rect, WSAD_POS WSAD);
private:
    QList<QWidget*> m_ListWidget;
    QList<QString> m_ListWidgetStr;
    QList<QStringList> m_ListWidgetStrList;
    QList<WSAD_POS> m_ListWidgetPos;

    int m_index = 0;

    QWidget* m_MainWindows;
    QLine targetLine;
};

#endif // TOURGUIDE_H

