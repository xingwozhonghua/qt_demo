#include "tourguide.h"

#include <QDebug>
TourGuide::TourGuide(QWidget *w, QWidget *parent) : QWidget(parent)
{
    m_MainWindows = w;
}

void TourGuide::onAddWidget(QWidget *w, QString title, QStringList contentList, int pos)
{
    m_ListWidget << w;
    m_ListWidgetStr << title;
    m_ListWidgetStrList << contentList;

    m_ListWidgetPos << WSAD_POS(pos);
}

void TourGuide::paintEvent(QPaintEvent *event)
{
    Q_UNUSED(event);

    if (m_index < 0 || m_index >= m_ListWidget.size())
        return;

    QPainter painter(this);
    painter.setRenderHint(QPainter::Antialiasing);

    QWidget *currentWidget = m_ListWidget[m_index];
    if (!currentWidget)
        return;


    // 获取QWidget在主窗体的位置
    QPoint guidePos = currentWidget->mapTo(m_MainWindows, QPoint(0, 0));
    QRect rect(guidePos.x(), guidePos.y(), currentWidget->width(), currentWidget->height());
    //循环如果包含该ui就不绘制
    painter.setPen(Qt::NoPen);
    painter.setBrush(QColor(0, 0, 0, 100));

    // 当前widget的大小
    int fullWidth = this->width();
    int fullHeight = this->height();

    // 计算四周矩形区域
    QRect topRect(0, 0, fullWidth, rect.y());
    QRect bottomRect(0, rect.y() + rect.height(), fullWidth, fullHeight - (rect.y() + rect.height()));
    QRect leftRect(0, rect.y(), rect.x(), rect.height());
    QRect rightRect(rect.x() + rect.width(), rect.y(), fullWidth - (rect.x() + rect.width()), rect.height());

    // 设置笔刷
    painter.setBrush(QColor(0, 0, 0, 100));

    // 绘制四周矩形 把ui的位置进行空出来，绘制它的四个区域,仅支持规则ui，这样就不支持奇形怪状
    painter.drawRect(topRect);
    painter.drawRect(bottomRect);
    painter.drawRect(leftRect);
    painter.drawRect(rightRect);

    painter.setBrush(QColor(0, 0, 0, 0));

    QPen pen(Qt::green);

    //圈出来
    pen.setWidth(1);
    painter.setPen(pen);
    painter.drawRect(rect);

    drawArrows(painter, guidePos, currentWidget->rect(), m_ListWidgetPos.at(m_index));

    // 绘制矩形框和箭头
    // 绘制文字
    painter.setPen(Qt::white); // 设置文字颜色为白色
    QFont font = painter.font(); // 获取当前字体设置
    font.setPointSize(18); // 设置字体大小
    font.setBold(true); // 设置为粗体
    painter.setFont(font); // 应用设置后的字体
    // 获取文本的宽度和高度
    QRectF textRect = painter.boundingRect(QRectF(), Qt::AlignCenter, m_ListWidgetStr.at(m_index));

    // 计算文字应该绘制的位置，使其居中在窗口中央
    qreal x = (this->width() - textRect.width()) / 2;
    qreal y = (this->height() - textRect.height()) / 2;
    // 绘制标题文字
    painter.drawText(QPointF(x, y), m_ListWidgetStr.at(m_index));
    QStringList list = m_ListWidgetStrList.at(m_index);


    // 绘制子文字
    for (int i = 0 ; i < list.size(); ++i) {
        textRect = painter.boundingRect(QRectF(), Qt::AlignCenter, list.at(i));
        x = (this->width() - textRect.width()) / 2;
        y = (this->height() - textRect.height()) / 2 + ((i+1) * 40);
        font.setPointSize(12);
        font.setBold(false);
        painter.setFont(font); // 应用设置后的字体
        // 绘制标题文字
        painter.drawText(QPointF(x, y), list.at(i));
    }
}

void TourGuide::mousePressEvent(QMouseEvent *event)
{
    if (event->type() == QMouseEvent::MouseButtonPress) {
        this->update();
        qDebug() << m_index;
        if (m_index + 1 < m_ListWidget.size()) {
            m_index++;
        }
        else {
            this->close();
            m_MainWindows->show();
        }
    }
}

void TourGuide::drawArrows(QPainter &painter, QPoint pos, QRect rect, WSAD_POS WSAD)
{
    QPoint f1Start;
    QPoint f1Stop;
    QPoint f2Start;
    QPoint f2Stop;
    QPoint f3Start;
    QPoint f3Stop;
    if (WSAD == TOP_POS) {
        // 绘制箭头
        f1Start = QPoint(pos.x() + (rect.width() / 2), pos.y() - 5);
        f1Stop = QPoint(pos.x() + (rect.width() / 2) - 6, pos.y() - 12);
        //绘制两边的线
        f2Start = QPoint(pos.x() + (rect.width() / 2), pos.y() - 5);
        f2Stop = QPoint(pos.x() + (rect.width() / 2) + 6, pos.y() - 12);

        //绘制中间的线
        f3Start = QPoint(pos.x() + (rect.width() / 2), pos.y() - 5);
        f3Stop = QPoint(pos.x() + (rect.width() / 2), pos.y() - 20);
    }
    else if (WSAD == DOWN_PPOS) {
        // 绘制箭头
        f1Start = QPoint(pos.x() + (rect.width() / 2), pos.y() + rect.height() + 5);
        f1Stop = QPoint(pos.x() + (rect.width() / 2) - 6, pos.y() + rect.height() + 12);
        //绘制两边的线
        f2Start = QPoint(pos.x() + (rect.width() / 2), pos.y() + rect.height() + 5);
        f2Stop = QPoint(pos.x() + (rect.width() / 2) + 6, pos.y() + rect.height() + 12);

        //绘制中间的线
        f3Start = QPoint(pos.x() + (rect.width() / 2), pos.y() + rect.height() + 5);
        f3Stop = QPoint(pos.x() + (rect.width() / 2), pos.y() + rect.height() + 20);
    }
    else if (WSAD == LEFT_POS) {
        // 绘制箭头
        f1Start = QPoint(pos.x() - 5, pos.y() + (rect.height() / 2));
        f1Stop = QPoint(pos.x() - 12, pos.y() + 5);
        //绘制两边的线
        f2Start = QPoint(pos.x() - 5, pos.y() + (rect.height() / 2));
        f2Stop = QPoint(pos.x() - 12, pos.y() + (rect.height() / 2) + 6);

        //绘制中间的线
        f3Start = QPoint(pos.x() - 5, pos.y() + rect.height() / 2);
        f3Stop = QPoint(pos.x() - 20, pos.y() + rect.height() / 2);
    }
    else if (WSAD == RIGHT_POS) {
        // 绘制箭头
        f1Start = QPoint(pos.x() + rect.width() + 5, pos.y() + (rect.height() / 2));
        f1Stop = QPoint(pos.x() + rect.width() + 12, pos.y() + 5);
        //绘制两边的线
        f2Start = QPoint(pos.x() + rect.width() + 5, pos.y() + (rect.height() / 2));
        f2Stop = QPoint(pos.x() + rect.width() + 12, pos.y() + (rect.height() / 2) + 6);

        //绘制中间的线
        f3Start = QPoint(pos.x() + rect.width() + 5, pos.y() + rect.height() / 2);
        f3Stop = QPoint(pos.x() + rect.width() + 20, pos.y() + rect.height() / 2);
    }

    QPen pen(Qt::green);
    pen.setWidth(2);
    painter.setPen(pen);
    painter.drawLine(f1Start, f1Stop);
    painter.drawLine(f2Start, f2Stop);
    painter.drawLine(f3Start, f3Stop);
}


