#ifndef FILEEXPLORER_H
#define FILEEXPLORER_H

#include <QWidget>
#include <QPushButton>
#include <QMessageBox>
#include <QInputDialog>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QFileDialog>
#include <QFileIconProvider>
#include "TreeView.h"
class FileExplorer : public QWidget {
	Q_OBJECT

public:
	explicit FileExplorer(QWidget* parent = nullptr);

private:
	TreeView* m_treeView;
	QPushButton* m_addFileButton;
	QPushButton* m_addFolderButton;
	QPushButton* m_renameFolderButton;
	QPushButton* m_deleteButton;
	QPushButton* m_copyButton;
};

#endif // FILEEXPLORER_H