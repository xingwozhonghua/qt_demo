#include "FileExplorer.h"

FileExplorer::FileExplorer(QWidget* parent)
	: QWidget(parent)
{
	// 创建GUI控件
	m_treeView = new TreeView(this);
	m_addFileButton = new QPushButton(tr("Add File"), this);
	m_addFolderButton = new QPushButton(tr("Add Folder"), this);
	m_renameFolderButton = new QPushButton(tr("Rename Folder"), this);
	m_deleteButton = new QPushButton(tr("Delete"), this);
	m_copyButton = new QPushButton(tr("Copy"), this);

	// 设置GUI布局
	QVBoxLayout* layout = new QVBoxLayout(this);
	QHBoxLayout* buttonLayout = new QHBoxLayout();
	buttonLayout->addWidget(m_addFileButton);
	buttonLayout->addWidget(m_addFolderButton);
	buttonLayout->addWidget(m_renameFolderButton);
	buttonLayout->addWidget(m_deleteButton);
	buttonLayout->addWidget(m_copyButton);
	layout->addWidget(m_treeView);
	layout->addLayout(buttonLayout);

	// 初始化文件系统模型和视图
	m_treeView->setColumnWidth(0, 300);
	m_treeView->setHeaderHidden(true);
	m_treeView->setSelectionMode(QAbstractItemView::ExtendedSelection);

	// 连接信号和槽函数
	connect(m_addFileButton, SIGNAL(clicked()), m_treeView, SLOT(addFile()));
	connect(m_addFolderButton, SIGNAL(clicked()), m_treeView, SLOT(addFolder()));
	connect(m_renameFolderButton, SIGNAL(clicked()), m_treeView, SLOT(renameItem()));
	connect(m_deleteButton, SIGNAL(clicked()), m_treeView, SLOT(deleteItem()));
	connect(m_copyButton, SIGNAL(clicked()), m_treeView, SLOT(exportFiles()));
}