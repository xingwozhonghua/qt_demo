#include "FileExplorer.h"
#include <QtWidgets/QApplication>

int main(int argc, char *argv[])
{
	QApplication app(argc, argv);

	FileExplorer fileSystem;
	fileSystem.resize(640, 480);
	fileSystem.show();

	return app.exec();
}
