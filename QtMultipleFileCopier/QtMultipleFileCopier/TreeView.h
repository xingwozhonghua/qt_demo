#ifndef TREEVIEW_H
#define TREEVIEW_H

#include <QTreeView>
#include <QStandardItemModel>
#include <QDragEnterEvent>

class TreeView : public QTreeView
{
	Q_OBJECT

public:
	explicit TreeView(QWidget *parent = nullptr);

protected:
	void dragEnterEvent(QDragEnterEvent* event) override;
	void dropEvent(QDropEvent* event) override;

public slots:
	void addFolder();
	void addFile();
	void deleteItem();
	void renameItem();
	void exportFiles();


private:
	void exportSelectedItems(const QString& exportPath);
	bool promptOverwrite(const QString& filePath);
	void addSubFolder(const QString& folderPath, QStandardItem* parentItem);
	void addFileToFolder(const QString& filePath, QStandardItem* parentItem);
	void exportItemRecursively(const QModelIndex& parentIndex, QStandardItem* item, const QString& destinationFolderPath);


	QStandardItemModel m_model;
};
#endif // TREEVIEW_H
