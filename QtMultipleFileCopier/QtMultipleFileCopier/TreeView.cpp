#include "TreeView.h"
#include <QMenu>
#include <QFileDialog>
#include <QMessageBox>
#include <QInputDialog>
#include <QDebug>
#include <QMimeData>

TreeView::TreeView(QWidget *parent)
	: QTreeView(parent)
{
	// 设置模型
	setModel(&m_model);

	// 允许多选
	setSelectionMode(QAbstractItemView::ExtendedSelection);

	// 添加右键菜单
	setContextMenuPolicy(Qt::CustomContextMenu);
	connect(this, &QTreeView::customContextMenuRequested, [this](const QPoint& pos) {

		QMenu menu(this);
		QAction* addFolderAction = menu.addAction(tr("Add Folder"));
		QAction* addFileAction = menu.addAction(tr("Add File"));
		QAction* deleteAction = menu.addAction(tr("Delete"));
		QAction* renameAction = menu.addAction(tr("Rename"));
		menu.addSeparator();
		QAction* exportAction = menu.addAction(tr("Export..."));

		QAction* selectedItem = menu.exec(viewport()->mapToGlobal(pos));
		if (selectedItem == addFolderAction) {
			addFolder();
		}
		else if (selectedItem == addFileAction) {
			addFile();
		}
		else if (selectedItem == deleteAction) {
			deleteItem();
		}
		else if (selectedItem == renameAction) {
			renameItem();
		}
		else if (selectedItem == exportAction) {
			exportFiles();
		}
	});

	// 在构造函数中开启拖放功能
	setAcceptDrops(true);
	setDragEnabled(true);
	setDropIndicatorShown(true);
	setDragDropMode(QAbstractItemView::InternalMove);
}
void TreeView::addFolder()
{
	QModelIndexList selectedIndexes = selectionModel()->selectedIndexes();
	QModelIndex parentIndex;
	if (selectedIndexes.isEmpty()) {
		// 如果没有选择项，则使用根节点
		parentIndex = QModelIndex();
	}
	else {
		parentIndex = selectedIndexes.first().parent();
	}

	QString folderPath = QFileDialog::getExistingDirectory(this, tr("Add Folder"), QDir::homePath());
	if (!folderPath.isEmpty()) {
		// 获取文件夹名称
		QFileInfo fileInfo(folderPath);
		QString folderName = fileInfo.fileName();
		QString absoluteFilePath = fileInfo.absoluteFilePath();

		// 检查文件夹名称是否已存在于同级别的子项中
		QStandardItem* parentItem = m_model.itemFromIndex(parentIndex);
		if (!parentItem) {
			parentItem = m_model.invisibleRootItem();
		}

		int numChildren = parentItem->rowCount();
		for (int i = 0; i < numChildren; ++i) {
			if (parentItem->child(i)->text() == folderName) {
				QMessageBox::warning(this, tr("Add Folder"), tr("A subitem with the same name already exists."));
				return;
			}
		}

		// 创建新文件夹条目
		QStandardItem* folderItem = new QStandardItem(folderName);
		folderItem->setEditable(false);
		folderItem->setData(QVariant::fromValue<QString>("folder"), Qt::UserRole);
		folderItem->setData(QVariant::fromValue<QString>(absoluteFilePath), Qt::UserRole + 1);

		// 添加到模型中
		int row = (parentIndex.isValid()) ? parentIndex.row() + 1 : m_model.rowCount(); // 添加到父项后面或者根节点下
		QList<QStandardItem*> itemsList;
		itemsList << folderItem;
		if (parentIndex.isValid()) {
			QStandardItem *parentItem = m_model.itemFromIndex(parentIndex);
			if (parentItem) {
				parentItem->insertRow(row, itemsList);
			}
		}
		else {
			m_model.insertRow(row, itemsList);
		}

		// 获取子目录列表
		QDir dir(folderPath);
		QStringList subdirs = dir.entryList(QDir::Dirs | QDir::NoDotAndDotDot, QDir::Name);
		foreach(const QString& subdir, subdirs) {
			QString subfolderPath = folderPath + "/" + subdir;
			addSubFolder(subfolderPath, folderItem);
		}

		// 获取文件列表
		QStringList files = dir.entryList(QDir::Files, QDir::Name);
		foreach(const QString& file, files) {
			addFileToFolder(file, folderItem);
		}

		// 展开父级项
		expand(parentIndex);

		// 刷新视图并更新布局
		viewport()->update();
		updateGeometry();
	}
}

void TreeView::addSubFolder(const QString &folderPath, QStandardItem *parentItem)
{
	// 获取文件夹名称
	QFileInfo fileInfo(folderPath);
	QString folderName = fileInfo.fileName();
	QString absoluteFilePath = fileInfo.absoluteFilePath();


	// 创建新文件夹条目
	QStandardItem* folderItem = new QStandardItem(folderName);
	folderItem->setEditable(false);
	folderItem->setData(QVariant::fromValue<QString>("folder"), Qt::UserRole);
	folderItem->setData(QVariant::fromValue<QString>(absoluteFilePath), Qt::UserRole + 1);


	// 添加到父项中
	parentItem->appendRow(folderItem);

	// 获取子目录列表
	QDir dir(folderPath);
	QStringList subdirs = dir.entryList(QDir::Dirs | QDir::NoDotAndDotDot, QDir::Name);

	// 递归添加每个子目录
	foreach(const QString& subdir, subdirs) {
		QString subfolderPath = folderPath + "/" + subdir;
		addSubFolder(subfolderPath, folderItem);
	}

	// 添加该文件夹内的所有文件
	QStringList files = dir.entryList(QDir::Files, QDir::Name);
	foreach(const QString& file, files) {
		addFileToFolder(file, folderItem);
	}
}

void TreeView::addFileToFolder(const QString& filePath, QStandardItem* parentItem)
{
	// 获取文件名和父文件夹的路径
	QFileInfo fileInfo(filePath);
	QString fileName = fileInfo.fileName();
	QString folderPath = parentItem->data(Qt::UserRole + 1).toString();

	// 拼接得到文件的绝对路径
	QString absoluteFilePath = folderPath + "/" + fileName;

	// 创建新文件条目
	QStandardItem* fileItem = new QStandardItem(fileName);

	fileItem->setEditable(false);
	fileItem->setData(QVariant::fromValue<QString>("file"), Qt::UserRole);
	fileItem->setData(QVariant::fromValue<QString>(absoluteFilePath), Qt::UserRole + 1);

	// 添加到父项中
	parentItem->appendRow(fileItem);
}

void TreeView::addFile()
{
	QModelIndexList selectedIndexes = selectionModel()->selectedIndexes();
	QModelIndex parentIndex;
	if (selectedIndexes.isEmpty()) {
		// 如果没有选择项，则使用根节点
		parentIndex = QModelIndex();
	}
	else {
		parentIndex = selectedIndexes.first().parent();
	}

	QString filePath = QFileDialog::getOpenFileName(this, tr("Add File"), QDir::homePath());
	if (!filePath.isEmpty()) {
		// 获取文件名
		QFileInfo fileInfo(filePath);
		QString fileName = fileInfo.fileName();
		QString absoluteFilePath = fileInfo.absoluteFilePath();

		// 检查文件名是否已存在于同级别的子项中
		QStandardItem* parentItem = m_model.itemFromIndex(parentIndex);
		if (!parentItem) {
			parentItem = m_model.invisibleRootItem();
		}

		int numChildren = parentItem->rowCount();
		for (int i = 0; i < numChildren; ++i) {
			if (parentItem->child(i)->text() == fileName) {
				QMessageBox::warning(this, tr("Add File"), tr("A subitem with the same name already exists."));
				return;
			}
		}

		// 创建新文件条目
		QStandardItem* fileItem = new QStandardItem(fileName);
		fileItem->setEditable(false);
		fileItem->setData(QVariant::fromValue<QString>("file"), Qt::UserRole);
		fileItem->setData(QVariant::fromValue<QString>(absoluteFilePath), Qt::UserRole + 1);

		// 添加到模型中
		int row = (parentIndex.isValid()) ? parentIndex.row() + 1 : m_model.rowCount(); // 添加到父项后面或者根节点下
		QList<QStandardItem*> itemsList;
		itemsList << fileItem;
		if (parentIndex.isValid()) {
			QStandardItem *parentItem = m_model.itemFromIndex(parentIndex);
			if (parentItem) {
				parentItem->insertRow(row, itemsList);
			}
		}
		else {
			m_model.insertRow(row, itemsList);
		}

		// 刷新视图并更新布局
		viewport()->update();
		updateGeometry();
	}
}

void TreeView::deleteItem()
{
	QModelIndexList selectedIndexes = selectionModel()->selectedIndexes();
	for (const QModelIndex& index : selectedIndexes) {
		// 删除条目和其所有子项
		m_model.removeRow(index.row(), index.parent());
	}
}

void TreeView::renameItem()
{
	QModelIndex index = currentIndex();
	if (!index.isValid()) {
		return;
	}

	// 获取当前选定项的文本和数据
	QString oldName = index.data(Qt::DisplayRole).toString();

	bool ok;
	QString newName = QInputDialog::getText(this, tr("Rename"), tr("New Name:"), QLineEdit::Normal, oldName, &ok);
	if (ok && !newName.isEmpty() && newName != oldName) {
		// 更改条目名称
		m_model.setData(index, newName, Qt::DisplayRole);
	}
}
void TreeView::exportFiles()
{
	QString exportFolderPath = QFileDialog::getExistingDirectory(this, tr("Export Files"), QDir::homePath());
	if (!exportFolderPath.isEmpty()) {
		// 获取根节点
		QStandardItem* rootItem = m_model.invisibleRootItem();
		QModelIndex rootIndex = rootItem->index();

		// 递归地导出每个项及其所有子项
		exportItemRecursively(rootIndex, rootItem, exportFolderPath);

		QMessageBox::information(this, tr("Export Files"), tr("Files exported successfully."));
	}
}

void TreeView::exportItemRecursively(const QModelIndex& itemIndex, QStandardItem* item, const QString& destinationFolderPath)
{
	QString itemName = item->data(Qt::DisplayRole).toString(); // 获取项名称

	if (item->data(Qt::UserRole).toString() == "file") {
		// 如果当前项是文件，则将其复制到导出文件夹中
		QString absoluteFilePath = item->data(Qt::UserRole + 1).toString();
		QFileInfo fileInfo(absoluteFilePath);
		QString destinationFilePath = destinationFolderPath + "/" + itemName; // 使用项名称作为文件名
		QFile::copy(absoluteFilePath, destinationFilePath);
	}
	else {
		// 如果当前项是文件夹，则创建新的导出文件夹并递归导出其中的所有子项
		QString newDestinationFolderPath = destinationFolderPath + "/" + itemName; // 使用项名称作为新导出文件夹的名称
		QDir().mkdir(newDestinationFolderPath);

		int numRows = item->rowCount();
		for (int i = 0; i < numRows; ++i) {
			QStandardItem* childItem = item->child(i);
			if (childItem) {
				QModelIndex childIndex = childItem->index(); // 获取孩子的索引
				exportItemRecursively(childIndex, childItem, newDestinationFolderPath); // 导出子项和子文件夹
			}
		}

		// 导出文件夹内部的文件
		for (int i = 0; i < numRows; ++i) {
			QStandardItem* childItem = item->child(i);
			if (childItem && childItem->data(Qt::UserRole).toString() == "file") {
				QString absoluteFilePath = childItem->data(Qt::UserRole + 1).toString();
				QFileInfo fileInfo(absoluteFilePath);
				QString destinationFilePath = newDestinationFolderPath + "/" + childItem->data(Qt::DisplayRole).toString(); // 使用子项名称作为文件名
				QFile::copy(absoluteFilePath, destinationFilePath);
			}
		}
	}
}
bool TreeView::promptOverwrite(const QString& filePath)
{
	// 检查是否存在同名文件
	if (QFile::exists(filePath)) {
		QMessageBox::StandardButton result = QMessageBox::warning(this, tr("File Already Exists"), tr("A file with the name \"%1\" already exists in the export folder. Do you want to overwrite it?").arg(QFileInfo(filePath).fileName()), QMessageBox::Yes | QMessageBox::No);
		return result == QMessageBox::Yes;
	}

	return true;
}
void TreeView::dragEnterEvent(QDragEnterEvent* event)
{
	if (event->mimeData()->hasUrls()) {
		bool allFilesOrDirs = true;
		QList<QUrl> urls = event->mimeData()->urls();
		foreach(const QUrl& url, urls) {
			QString filePath = url.toLocalFile();
			QFileInfo fileInfo(filePath);
			if (!fileInfo.isFile() && !fileInfo.isDir()) {
				allFilesOrDirs = false;
				break;
			}
			if (fileInfo.suffix().toLower() == "lnk") {
				allFilesOrDirs = false;
				break;
			}
		}
		if (allFilesOrDirs) {
			event->acceptProposedAction();
			event->accept(); // 确保允许 dropEvent() 被调用
		}
	}
}

void TreeView::dropEvent(QDropEvent* event)
{
	QList<QUrl> urls = event->mimeData()->urls();
	foreach(const QUrl& url, urls) {
		QString filePath = url.toLocalFile();
		QFileInfo fileInfo(filePath);
		if (fileInfo.isFile()) {
			addFileToFolder(filePath, m_model.invisibleRootItem());
		}
		else if (fileInfo.isDir()) {
			// 如果拖入的是文件夹，则添加文件夹和其子文件夹和文件到树控件中
			addSubFolder(filePath, m_model.invisibleRootItem());
		}
	}
}